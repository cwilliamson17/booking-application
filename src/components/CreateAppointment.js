import React from "react";
import { Link } from "react-router-dom";

import { connect } from "react-redux";

import { staff } from "../mockData/staffMembers";
import { addAppointment } from "../actions";

class CreateAppointment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nameInput: "",
      mobileNumberInput: "",
      selectedStaffMember: ""
    };
  }

  onNameValueChange = e => {
    this.setState({ nameInput: e.target.value });
  };

  onMobileValueChange = e => {
    this.setState({ mobileNumberInput: e.target.value });
  };

  setStaffMember = name => {
    this.setState({ selectedStaffMember: name });
  };

  listAvailableStaff = staffList => {
    return staffList.map(staffMember => {
      return (
        <div key={staffMember.id}>
          <button
            onClick={e => {
              e.preventDefault();
              this.setStaffMember(staffMember.name);
            }}
          >
            {staffMember.name}
          </button>
        </div>
      );
    });
  };

  componentDidUpdate() {
    console.log("State:", this.props.appointments);
    console.log("Props:", this.buildApptObjectFromState());
  }

  buildApptObjectFromState = () => {
    const staffMember = this.state.selectedStaffMember;
    const name = this.state.nameInput;
    const mobile = this.state.mobileNumberInput;
    return {
      id: 1,
      worker: staffMember,
      name: name,
      mobile: mobile,
      order: 5
    };
  };

  render() {
    const { createAppointment } = this.props;
    return (
      <div>
        <form>
          <div>
            Name
            <input
              value={this.state.nameInput}
              type="text"
              onChange={this.onNameValueChange}
            />
          </div>
          <div>
            Mobile
            <input
              value={this.state.mobileNumberInput}
              type="number"
              onChange={this.onMobileValueChange}
            />
          </div>
          <div>{this.listAvailableStaff(staff)}</div>
          <button
            onClick={e => {
              e.preventDefault();
              createAppointment(this.buildApptObjectFromState());
            }}
          >
            Create Appointment
          </button>
        </form>
        <Link to="/">Home</Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appointments: state.appointments
  };
};

export default connect(
  mapStateToProps,
  {
    createAppointment: addAppointment
  }
)(CreateAppointment);
