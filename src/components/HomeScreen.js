import React from "react";
import { Link } from "react-router-dom";

class HomeScreen extends React.Component {
  renderMenuOptions = options => {
    return options.map(option => {
      return (
        <div key={option.linkTitle}>
          <Link to={option.toURL}>{option.linkTitle}</Link>
        </div>
      );
    });
  };

  render() {
    let opt = [
      { toURL: "/create", linkTitle: "Create Appointment" },
      { toURL: "/change", linkTitle: "Change Appointment" },
      { toURL: "/remove", linkTitle: "Remove Appointment" },
      { toURL: "/view", linkTitle: "View Appointments" }
    ];
    return this.renderMenuOptions(opt);
  }
}

export default HomeScreen;
