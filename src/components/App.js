import React from "react";
import { BrowserRouter, Route } from "react-router-dom";

import HomeScreen from "./HomeScreen";
import CreateAppointment from "./CreateAppointment";
import ViewAppointments from "./ViewAppointments";

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Route exact path="/" component={HomeScreen} />
        <Route exact path="/create" component={CreateAppointment} />
        <Route exact path="/view" component={ViewAppointments} />
      </BrowserRouter>
    );
  }
}

export default App;
