import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class ViewAppointments extends React.Component {
  renderAppointments = () => {
    console.log("renderappointments called");
    const { appointments } = this.props;
    console.log(appointments);

    if (appointments.length === 0) {
      return <div>No appointments</div>;
    }

    const appStyle = {
      border: "1px solid black"
    };

    return appointments.map((appt, index) => {
      return (
        <div style={appStyle}>
          <div>Id: {appt.id}</div>
          <div>Name: {appt.customerName}</div>
          <div>Mobile: {appt.customerMobile}</div>
          <div>Worker: {appt.worker}</div>
        </div>
      );
    });
  };

  render() {
    console.log("View props:", this.props.appointments);
    return (
      <div>
        <h1>Appointments</h1>
        <div>{this.renderAppointments()}</div>
        <Link to="/">Home</Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appointments: state.appointments
  };
};

export default connect(mapStateToProps)(ViewAppointments);
