export const addAppointment = appt => {
  return {
    type: "CREATE_APPOINTMENT",
    payload: {
      id: appt.id,
      worker: appt.worker,
      customerName: appt.name,
      customerMobile: appt.mobile,
      orderInQueue: appt.order
    }
  };
};

export const removeAppointment = appt => {
  return {
    type: "REMOVE_APPOINTMENT",
    payload: {
      customerName: appt.name,
      customerMobile: appt.mobile
    }
  };
};
