import { updateAppointments } from "./updateAppointments";
import { combineReducers } from "redux";

export const reducers = combineReducers({
  appointments: updateAppointments
});
