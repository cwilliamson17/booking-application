export const updateAppointments = (appointments = [], action) => {
  console.log("in reducer");
  console.log(action.payload);
  switch (action.type) {
    case "CREATE_APPOINTMENT":
      console.log("create appointment called");
      return [...appointments, action.payload];
    case "REMOVE_APPOINTMENT":
      const filteredAppointments = appointments.filter(appt => {
        return appt.id !== action.payload.id;
      });
      return filteredAppointments;

    default:
      return appointments;
  }
};
