Simple Booking App implemented with React & Redux

Currently Bootstrapped with Create-React-App

Current Functionality:

1. Add Appointments
2. 

Roadmap:

1. Allow Appointments to be viewed 
2. Allow appointments to be removed
3. Add BEM css classes 
4. Add CSS
5. Add Authentication for store owner using node and express (OAUTH)
6. Add webpack config to move away from create-react-app
7. Allow store owner to add workers which clients can choose from when booking
8. Allow view appointments based on worker
9. Allow appointments to be created without choice of specific worker (fastest appointment available queueing)
10. Add GraphQL/Mongo backend to allow:
11.     - estimates of customer wait time based on historical worker data
12.     - customer data to be gathered

